#!/usr/bin/env python

"""
First, a few callback functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Santa Mònica conversation bot.
Send /start to initiate the conversation.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import logging

from telegram import InlineKeyboardMarkup, InlineKeyboardButton, ReplyKeyboardRemove, ReplyKeyboardRemove, Update
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    ConversationHandler,
    CallbackContext,
    CallbackQueryHandler,
)

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)
logger = logging.getLogger(__name__)

QUESTION1, QUESTION2, QUESTION3 = range(3)


def question1(update: Update, context: CallbackContext) -> int:

    user = update.message.from_user
    logger.info("----- Visitante %s %s (%s)", user.first_name, user.last_name, user.username)

    text = (
        '¡Hola! Soy la robot del Santa Mònica y quiero proponerte un paseo.\n\n'
        'Manda /cancel para terminar la conversación.'
    )
    context.bot.send_message(chat_id=update.effective_chat.id, text=text)

    update.message.reply_text(
        '¿Te Animas?',

        reply_markup = InlineKeyboardMarkup([
            [
                InlineKeyboardButton(text=str('Sí'), callback_data=str('Sí')),
                InlineKeyboardButton(text=str('No'), callback_data=str('No')),
            ],
        ])
    )

    return QUESTION1


def question2(update: Update, context: CallbackContext) -> int:
    """Parses the CallbackQuery and updates the message text."""
    query = update.callback_query
    query.answer()

    logger.info("¿Te Animas? %s", query.data)

    query.edit_message_text(text="¿Te Animas?")
    context.bot.send_message(chat_id=update.effective_chat.id, text=query.data)

    if query.data == 'Sí':
        #context.bot.send_message(chat_id=update.effective_chat.id, text='¡Qué bien! ¿Dónde estás ahora?')
        query.message.reply_text(
            '¡Qué bien! ¿Dónde estás ahora?',
            reply_markup = InlineKeyboardMarkup([
                [
                    InlineKeyboardButton(text=str('Casa'), callback_data=str('Casa')),
                    InlineKeyboardButton(text=str('Santa Mònica'), callback_data=str('Santa Mònica')),
                ],
            ])
        )
        return QUESTION2

    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text="bueno, ¡hasta pronto!")
        return ConversationHandler.END


def question3(update: Update, context: CallbackContext) -> int:
    """Parses the CallbackQuery and updates the message text."""
    query = update.callback_query
    query.answer()

    logger.info("¿Dónde estás ahora? %s", query.data)

    context.bot.send_message(chat_id=update.effective_chat.id, text=query.data)
    query.edit_message_text(text="¿Dónde estás ahora?")

    if query.data == 'Santa Mònica':
        query.edit_message_text(text="¡Qué bien! ¿Dónde estás ahora?")
        context.bot.send_message(chat_id=update.effective_chat.id, text='Bien hecho. Salir afuera es salir de ti misma! Ahora vamos a la Sala de Espera')
        return QUESTION3

    else:
        query.edit_message_text(text="¡Qué bien! ¿Dónde estás ahora?")
        context.bot.send_message(chat_id=update.effective_chat.id, text='uy, entonces hablamos cuando llegues al Centro. Quedamos en la Sala de espera!')
        return ConversationHandler.END


def cancel(update: Update, context: CallbackContext) -> int:
    """Cancels and ends the conversation."""
    #logger.info("Usuario %s canceló conversación.", user.first_name)
    update.message.reply_text(
        'Adeu! Espero verte un día de estos...', reply_markup=ReplyKeyboardRemove()
    )

    return ConversationHandler.END


def main() -> None:
    """Run the bot."""
    # Create the Updater and pass it your bot's token.
    updater = Updater("Token")

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # Add conversation handler
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', question1)],
        states={
            QUESTION1: [CallbackQueryHandler(question2)],
            QUESTION2: [CallbackQueryHandler(question3)],
            QUESTION3: [CommandHandler('cancel', cancel)],
        },
        fallbacks=[CommandHandler('cancel', cancel)],
    )

    dispatcher.add_handler(conv_handler)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
